package com.myarchway.mikhail.sign;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.WindowManager;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

/**
 * Created by mihailpolovnev on 26/11/16.
 */

public class CartoonActivity extends Activity implements CameraBridgeViewBase.CvCameraViewListener2  {
    private static final String TAG = "OCVSample::Activity";

    private CameraBridgeViewBase mOpenCvCameraView;
    private boolean              mIsJavaCamera = true;
    private MenuItem mItemSwitchCamera = null;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);

        // Load ndk built module, as specified
        // in moduleName in build.gradle
        //System.loadLibrary("openCVLibrary310");
        OpenCVLoader.initDebug();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_cartoon);

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.camera_view);

        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);

        mOpenCvCameraView.setCvCameraViewListener(this);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        disableCamera();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        disableCamera();
        if (reusing1!=null) {
            reusing1.release();
            reusing2.release();
            reusing3.release();
        }
    }

    public void disableCamera() {
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
    }

    public void onCameraViewStopped() {
    }


    Mat reusing1 = null;
    Mat reusing2 = null;
    Mat reusing3 = null;
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {

        if (reusing1==null) {
            reusing1 = new Mat();
            reusing2 = new Mat();
            reusing3 = new Mat();
        }

        Mat mRgba = inputFrame.rgba();
        //Mat mGray = inputFrame.gray();

        Mat rgb = reusing1;
        Imgproc.cvtColor(mRgba,rgb,Imgproc.COLOR_RGBA2RGB);

        Mat downsampled = reusing2;
        Imgproc.pyrDown(rgb, downsampled);

        Mat downFiltered = reusing3;
        Imgproc.bilateralFilter(downsampled,downFiltered,9,9,7);

        Mat upsampled = reusing1;
        Imgproc.pyrUp(downFiltered,upsampled);

        Mat gray = reusing2;
        Imgproc.cvtColor(upsampled,gray,Imgproc.COLOR_RGB2GRAY);

        Mat blur = reusing3;
        Imgproc.medianBlur(gray,blur,5);

        Mat edge = reusing2;
        Imgproc.adaptiveThreshold(blur,edge,255,Imgproc.ADAPTIVE_THRESH_MEAN_C,Imgproc.THRESH_BINARY,9,2);

        Mat edgeRgb = reusing3;
        Imgproc.cvtColor(edge,edgeRgb,Imgproc.COLOR_GRAY2RGB);

        Mat out = reusing2;
        Core.bitwise_and(upsampled,edgeRgb,out);

        /*
        Mat edges = reusing1;
        Imgproc.adaptiveThreshold(mGray,edges,255,Imgproc.ADAPTIVE_THRESH_MEAN_C,Imgproc.THRESH_BINARY,9,2);

        Mat edgesRgba = reusing2;
        Imgproc.cvtColor(edges,edgesRgba,Imgproc.COLOR_GRAY2RGBA);

        Mat rgbBlurred = reusing1;
        Imgproc.medianBlur(mRgba,rgbBlurred,7);

        Mat out = reusing3;    // reusing the mat
        Core.bitwise_and(edgesRgba,rgbBlurred,out);*/

        return out;
    }

}
